Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: teseq
Upstream-Contact: bug-teseq@gnu.org
Source: http://www.gnu.org/software/teseq

Files: *
Copyright: 2008-2013 Micah Cowan <micah@addictivecode.org>
License: GPL-3+

Files: doc/teseq.info
       doc/teseq.texi
Copyright: 2008-2013 Micah Cowan <micah@addictivecode.org>
License: GFDL-1.2+

Files: ChangeLog
       NEWS
       README
       reseq.in
       tests/run.in
Copyright: 2008-2013 Micah Cowan <micah@addictivecode.org>
License: FSFAP

Files: debian/*
Copyright: 2009      Ryan Niebur <ryanryan52@gmail.com>
           2012      Gregor Herrmann <gregoa@debian.org>
           2013      Bill Allombert <ballombe@debian.org>
           2020      Boyuan Yang <byang@debian.org>
           2022-2023 Marcos Talau <talau@debian.org>
License: GPL-3+

License: GFDL-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
 Texts.
 .
 On Debian GNU/Linux systems, the complete text of the GNU Free
 Documentation License can be found in
 `/usr/share/common-licenses/GFDL-1.2'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU General Public License
 Version 3 can be found in the /usr/share/common-licenses/GPL-3 file.

License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.
